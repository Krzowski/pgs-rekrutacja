Autor: Jakub Krzowski

Program pozwalający na wpisanie danych osobowych z możliwością powrotu do poprzedniego widoku i poprawy danych.

Widoki to odpowiednio pojawiające się panele. Sprawdzanie poprawności wpisanych danych oparte zostało na wyrażeniach Regex.