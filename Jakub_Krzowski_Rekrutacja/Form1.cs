﻿using System;
using System.Windows.Forms;

namespace Jakub_Krzowski_Rekrutacja
{
   
    public partial class Form1 : Form
    {
        private readonly PersonalInfo _personalInfo = new PersonalInfo();
        public Form1()
        {
           InitializeComponent();
        }


        private void NextButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ContainerPanel.Controls.Count; i++)
            {
                
                if (!ContainerPanel.Controls[i].Visible) continue;
                BackButton.Visible = true;
           
                if (IsLastView(i))
                    break;

                GetData();

                if (!_personalInfo.Validate(i)) continue;

                if (IsLastViewCollectingData(i))
                    ShowData();

                ContainerPanel.Controls[i].Visible = false;
                ContainerPanel.Controls[i + 1].Visible = true;
                break;
            }
        }

       

        private bool IsLastView(int i)
        {
            return i == ContainerPanel.Controls.Count - 1;
        }

        private bool IsLastViewCollectingData(int i)
        {
            return i == ContainerPanel.Controls.Count - 2;
        }

        private void GetData()
        {
            _personalInfo.Name = NameTextBox.Text;
            _personalInfo.Surname = SurnameTextBox.Text;
            _personalInfo.Address = new Address(CityTextBox.Text,ZipCodeTextBox.Text,HomeNoTextBox.Text,StreetTextBox.Text);
            _personalInfo.PhoneNumber = PhoneNoTextBox.Text;
        }
        private void ShowData()
        {
            EndNameLabel.Text = _personalInfo.Name;
            EndViewSurnameLabel.Text = _personalInfo.Surname;
            EndViewPhoneNoLabel.Text = _personalInfo.PhoneNumber;
            EndViewCityLabel.Text = _personalInfo.Address.City;
            EndViewZipCodeLabel.Text = _personalInfo.Address.ZipCode;
            EndViewHomeNoLabel.Text = _personalInfo.Address.HomeNumber;
            EndViewStreetLabel.Text = _personalInfo.Address.Street;
        }
        private void BackButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ContainerPanel.Controls.Count; i++)
            {
                if (!ContainerPanel.Controls[i].Visible) continue;
                if (i == 0)
                    break;
              
                ContainerPanel.Controls[i].Visible = false;
                ContainerPanel.Controls[i - 1].Visible = true;
                break;
            }
        }
    }
}
