﻿using System;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Jakub_Krzowski_Rekrutacja
{
    class PersonalInfo
    {
       
        public string Name { get; set; }
        public string Surname { get; set; }
        public Address Address { get; set; }
        public string PhoneNumber { get; set; }

        public override string ToString()
        {
            return Name + " " + Surname + " " + PhoneNumber;
        }

        public bool Validate(int opendPanel)
        {
            switch (opendPanel)
            {
                case 0:
                    return true;
                case 1:
                    return ValidateName();

                case 2:
                    return ValidateSurname();

                case 3:
                    return Address.Validate();

                case 4:
                    return ValidatePhoneNo();
                default:
                    return false;

            }
             
        }

        private bool ValidateName()
        {
            Match match = Regex.Match(Name, @"(?i)^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+$");
            if (!match.Success)
            {
                MessageBox.Show("Bład w imieniu");
                return false;
            }
            return true;
        }
        private bool ValidateSurname()
        {
            Match match = Regex.Match(Surname, @"(?i)^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+$");
            if (!match.Success)
            {
                MessageBox.Show("Bład w nazwisku");
                return false;
            }
            return true;
        }

        private bool ValidatePhoneNo()
        {
            if (PhoneNumber.Length != 9)
            {
                MessageBox.Show("Zła długość numeru telefonu");
                return false;
            }

            Match match = Regex.Match(PhoneNumber, @"(?i)^[0-9]+$");

            if (!match.Success)
            {
                MessageBox.Show("Numer telefonu powinien zaweirać tylko cyfry");
                return false;
            }
            return true;
        }
    }
}
