﻿namespace Jakub_Krzowski_Rekrutacja
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonPanel = new System.Windows.Forms.Panel();
            this.BackButton = new System.Windows.Forms.Button();
            this.NextButton = new System.Windows.Forms.Button();
            this.FirstViewPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.ThirdViewPanel = new System.Windows.Forms.Panel();
            this.ThirdViewLabel = new System.Windows.Forms.Label();
            this.SurnameLabel = new System.Windows.Forms.Label();
            this.SurnameTextBox = new System.Windows.Forms.TextBox();
            this.SecondViewPanel = new System.Windows.Forms.Panel();
            this.SecondViewLabel = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.NameLable = new System.Windows.Forms.Label();
            this.ContainerPanel = new System.Windows.Forms.Panel();
            this.FourthViewPanel = new System.Windows.Forms.Panel();
            this.HomeNoTextBox = new System.Windows.Forms.TextBox();
            this.HomeNoLabel = new System.Windows.Forms.Label();
            this.ZipCodeTextBox = new System.Windows.Forms.TextBox();
            this.CityTextBox = new System.Windows.Forms.TextBox();
            this.StreetTextBox = new System.Windows.Forms.TextBox();
            this.ZipCodeLabel = new System.Windows.Forms.Label();
            this.CityLabel = new System.Windows.Forms.Label();
            this.StreetLabel = new System.Windows.Forms.Label();
            this.AddressLabel = new System.Windows.Forms.Label();
            this.FourthViewLabel = new System.Windows.Forms.Label();
            this.FifthViewPanel = new System.Windows.Forms.Panel();
            this.PhoneNoTextBox = new System.Windows.Forms.TextBox();
            this.PhoneNoLabel = new System.Windows.Forms.Label();
            this.FifthViewLabel = new System.Windows.Forms.Label();
            this.EndViewPanel = new System.Windows.Forms.Panel();
            this.EndViewHomeNoLabel = new System.Windows.Forms.Label();
            this.EndViewZipCodeLabel = new System.Windows.Forms.Label();
            this.EndViewStreetLabel = new System.Windows.Forms.Label();
            this.EndViewSurnameLabel = new System.Windows.Forms.Label();
            this.EndViewCityLabel = new System.Windows.Forms.Label();
            this.EndViewPhoneNoLabel = new System.Windows.Forms.Label();
            this.EndNameLabel = new System.Windows.Forms.Label();
            this.EndViewLabel = new System.Windows.Forms.Label();
            this.ButtonPanel.SuspendLayout();
            this.FirstViewPanel.SuspendLayout();
            this.ThirdViewPanel.SuspendLayout();
            this.SecondViewPanel.SuspendLayout();
            this.ContainerPanel.SuspendLayout();
            this.FourthViewPanel.SuspendLayout();
            this.FifthViewPanel.SuspendLayout();
            this.EndViewPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonPanel
            // 
            this.ButtonPanel.Controls.Add(this.BackButton);
            this.ButtonPanel.Controls.Add(this.NextButton);
            this.ButtonPanel.Location = new System.Drawing.Point(12, 320);
            this.ButtonPanel.Name = "ButtonPanel";
            this.ButtonPanel.Size = new System.Drawing.Size(452, 59);
            this.ButtonPanel.TabIndex = 10;
            // 
            // BackButton
            // 
            this.BackButton.Location = new System.Drawing.Point(269, 24);
            this.BackButton.Name = "BackButton";
            this.BackButton.Size = new System.Drawing.Size(75, 23);
            this.BackButton.TabIndex = 1;
            this.BackButton.Text = "Back";
            this.BackButton.UseVisualStyleBackColor = true;
            this.BackButton.Click += new System.EventHandler(this.BackButton_Click);
            // 
            // NextButton
            // 
            this.NextButton.Location = new System.Drawing.Point(350, 24);
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(75, 23);
            this.NextButton.TabIndex = 0;
            this.NextButton.Text = "Next";
            this.NextButton.UseVisualStyleBackColor = true;
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // FirstViewPanel
            // 
            this.FirstViewPanel.Controls.Add(this.label1);
            this.FirstViewPanel.Location = new System.Drawing.Point(3, 3);
            this.FirstViewPanel.Name = "FirstViewPanel";
            this.FirstViewPanel.Size = new System.Drawing.Size(452, 302);
            this.FirstViewPanel.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(168, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Witaj!";
            // 
            // ThirdViewPanel
            // 
            this.ThirdViewPanel.Controls.Add(this.ThirdViewLabel);
            this.ThirdViewPanel.Controls.Add(this.SurnameLabel);
            this.ThirdViewPanel.Controls.Add(this.SurnameTextBox);
            this.ThirdViewPanel.Location = new System.Drawing.Point(-3, -4);
            this.ThirdViewPanel.Name = "ThirdViewPanel";
            this.ThirdViewPanel.Size = new System.Drawing.Size(473, 321);
            this.ThirdViewPanel.TabIndex = 1;
            this.ThirdViewPanel.Visible = false;
            // 
            // ThirdViewLabel
            // 
            this.ThirdViewLabel.AutoSize = true;
            this.ThirdViewLabel.Location = new System.Drawing.Point(32, 23);
            this.ThirdViewLabel.Name = "ThirdViewLabel";
            this.ThirdViewLabel.Size = new System.Drawing.Size(58, 13);
            this.ThirdViewLabel.TabIndex = 0;
            this.ThirdViewLabel.Text = "Krok drugi:";
            // 
            // SurnameLabel
            // 
            this.SurnameLabel.AutoSize = true;
            this.SurnameLabel.Location = new System.Drawing.Point(77, 71);
            this.SurnameLabel.Name = "SurnameLabel";
            this.SurnameLabel.Size = new System.Drawing.Size(84, 13);
            this.SurnameLabel.TabIndex = 1;
            this.SurnameLabel.Text = "Podaj nazwisko:";
            // 
            // SurnameTextBox
            // 
            this.SurnameTextBox.Location = new System.Drawing.Point(167, 68);
            this.SurnameTextBox.Name = "SurnameTextBox";
            this.SurnameTextBox.Size = new System.Drawing.Size(138, 20);
            this.SurnameTextBox.TabIndex = 2;
            // 
            // SecondViewPanel
            // 
            this.SecondViewPanel.Controls.Add(this.SecondViewLabel);
            this.SecondViewPanel.Controls.Add(this.NameTextBox);
            this.SecondViewPanel.Controls.Add(this.NameLable);
            this.SecondViewPanel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SecondViewPanel.Location = new System.Drawing.Point(3, 4);
            this.SecondViewPanel.Name = "SecondViewPanel";
            this.SecondViewPanel.Size = new System.Drawing.Size(452, 299);
            this.SecondViewPanel.TabIndex = 2;
            this.SecondViewPanel.Visible = false;
            // 
            // SecondViewLabel
            // 
            this.SecondViewLabel.AutoSize = true;
            this.SecondViewLabel.Location = new System.Drawing.Point(26, 28);
            this.SecondViewLabel.Name = "SecondViewLabel";
            this.SecondViewLabel.Size = new System.Drawing.Size(75, 13);
            this.SecondViewLabel.TabIndex = 2;
            this.SecondViewLabel.Text = "Krok pierwszy:";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(143, 86);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(136, 20);
            this.NameTextBox.TabIndex = 1;
            // 
            // NameLable
            // 
            this.NameLable.AutoSize = true;
            this.NameLable.Location = new System.Drawing.Point(78, 86);
            this.NameLable.Name = "NameLable";
            this.NameLable.Size = new System.Drawing.Size(59, 13);
            this.NameLable.TabIndex = 0;
            this.NameLable.Text = "Podaj Imię:";
            // 
            // ContainerPanel
            // 
            this.ContainerPanel.Controls.Add(this.FirstViewPanel);
            this.ContainerPanel.Controls.Add(this.SecondViewPanel);
            this.ContainerPanel.Controls.Add(this.ThirdViewPanel);
            this.ContainerPanel.Controls.Add(this.FourthViewPanel);
            this.ContainerPanel.Controls.Add(this.FifthViewPanel);
            this.ContainerPanel.Controls.Add(this.EndViewPanel);
            this.ContainerPanel.Location = new System.Drawing.Point(4, 1);
            this.ContainerPanel.Name = "ContainerPanel";
            this.ContainerPanel.Size = new System.Drawing.Size(476, 322);
            this.ContainerPanel.TabIndex = 0;
            // 
            // FourthViewPanel
            // 
            this.FourthViewPanel.Controls.Add(this.HomeNoTextBox);
            this.FourthViewPanel.Controls.Add(this.HomeNoLabel);
            this.FourthViewPanel.Controls.Add(this.ZipCodeTextBox);
            this.FourthViewPanel.Controls.Add(this.CityTextBox);
            this.FourthViewPanel.Controls.Add(this.StreetTextBox);
            this.FourthViewPanel.Controls.Add(this.ZipCodeLabel);
            this.FourthViewPanel.Controls.Add(this.CityLabel);
            this.FourthViewPanel.Controls.Add(this.StreetLabel);
            this.FourthViewPanel.Controls.Add(this.AddressLabel);
            this.FourthViewPanel.Controls.Add(this.FourthViewLabel);
            this.FourthViewPanel.Location = new System.Drawing.Point(5, 3);
            this.FourthViewPanel.Name = "FourthViewPanel";
            this.FourthViewPanel.Size = new System.Drawing.Size(449, 299);
            this.FourthViewPanel.TabIndex = 4;
            this.FourthViewPanel.Visible = false;
            // 
            // HomeNoTextBox
            // 
            this.HomeNoTextBox.Location = new System.Drawing.Point(215, 130);
            this.HomeNoTextBox.Name = "HomeNoTextBox";
            this.HomeNoTextBox.Size = new System.Drawing.Size(134, 20);
            this.HomeNoTextBox.TabIndex = 10;
            // 
            // HomeNoLabel
            // 
            this.HomeNoLabel.AutoSize = true;
            this.HomeNoLabel.Location = new System.Drawing.Point(2, 130);
            this.HomeNoLabel.Name = "HomeNoLabel";
            this.HomeNoLabel.Size = new System.Drawing.Size(155, 13);
            this.HomeNoLabel.TabIndex = 9;
            this.HomeNoLabel.Text = "Podaj numer domu/mieszkania:";
            // 
            // ZipCodeTextBox
            // 
            this.ZipCodeTextBox.Location = new System.Drawing.Point(215, 211);
            this.ZipCodeTextBox.Name = "ZipCodeTextBox";
            this.ZipCodeTextBox.Size = new System.Drawing.Size(134, 20);
            this.ZipCodeTextBox.TabIndex = 8;
            // 
            // CityTextBox
            // 
            this.CityTextBox.Location = new System.Drawing.Point(215, 169);
            this.CityTextBox.Name = "CityTextBox";
            this.CityTextBox.Size = new System.Drawing.Size(134, 20);
            this.CityTextBox.TabIndex = 7;
            // 
            // StreetTextBox
            // 
            this.StreetTextBox.Location = new System.Drawing.Point(215, 93);
            this.StreetTextBox.Name = "StreetTextBox";
            this.StreetTextBox.Size = new System.Drawing.Size(134, 20);
            this.StreetTextBox.TabIndex = 6;
            // 
            // ZipCodeLabel
            // 
            this.ZipCodeLabel.AutoSize = true;
            this.ZipCodeLabel.Location = new System.Drawing.Point(51, 218);
            this.ZipCodeLabel.Name = "ZipCodeLabel";
            this.ZipCodeLabel.Size = new System.Drawing.Size(106, 13);
            this.ZipCodeLabel.TabIndex = 4;
            this.ZipCodeLabel.Text = "Podaj kod pocztowy:";
            // 
            // CityLabel
            // 
            this.CityLabel.AutoSize = true;
            this.CityLabel.Location = new System.Drawing.Point(87, 172);
            this.CityLabel.Name = "CityLabel";
            this.CityLabel.Size = new System.Drawing.Size(70, 13);
            this.CityLabel.TabIndex = 3;
            this.CityLabel.Text = "Podaj miasto:";
            // 
            // StreetLabel
            // 
            this.StreetLabel.AutoSize = true;
            this.StreetLabel.Location = new System.Drawing.Point(87, 93);
            this.StreetLabel.Name = "StreetLabel";
            this.StreetLabel.Size = new System.Drawing.Size(62, 13);
            this.StreetLabel.TabIndex = 2;
            this.StreetLabel.Text = "Podaj ulicę:";
            // 
            // AddressLabel
            // 
            this.AddressLabel.AutoSize = true;
            this.AddressLabel.Location = new System.Drawing.Point(85, 54);
            this.AddressLabel.Name = "AddressLabel";
            this.AddressLabel.Size = new System.Drawing.Size(72, 13);
            this.AddressLabel.TabIndex = 1;
            this.AddressLabel.Text = "Podaj addres:";
            // 
            // FourthViewLabel
            // 
            this.FourthViewLabel.AutoSize = true;
            this.FourthViewLabel.Location = new System.Drawing.Point(5, 6);
            this.FourthViewLabel.Name = "FourthViewLabel";
            this.FourthViewLabel.Size = new System.Drawing.Size(60, 13);
            this.FourthViewLabel.TabIndex = 0;
            this.FourthViewLabel.Text = "Krok trzeci:";
            // 
            // FifthViewPanel
            // 
            this.FifthViewPanel.Controls.Add(this.PhoneNoTextBox);
            this.FifthViewPanel.Controls.Add(this.PhoneNoLabel);
            this.FifthViewPanel.Controls.Add(this.FifthViewLabel);
            this.FifthViewPanel.Location = new System.Drawing.Point(8, 3);
            this.FifthViewPanel.Name = "FifthViewPanel";
            this.FifthViewPanel.Size = new System.Drawing.Size(457, 313);
            this.FifthViewPanel.TabIndex = 5;
            this.FifthViewPanel.Visible = false;
            // 
            // PhoneNoTextBox
            // 
            this.PhoneNoTextBox.Location = new System.Drawing.Point(186, 89);
            this.PhoneNoTextBox.Name = "PhoneNoTextBox";
            this.PhoneNoTextBox.Size = new System.Drawing.Size(138, 20);
            this.PhoneNoTextBox.TabIndex = 2;
            // 
            // PhoneNoLabel
            // 
            this.PhoneNoLabel.AutoSize = true;
            this.PhoneNoLabel.Location = new System.Drawing.Point(69, 89);
            this.PhoneNoLabel.Name = "PhoneNoLabel";
            this.PhoneNoLabel.Size = new System.Drawing.Size(110, 13);
            this.PhoneNoLabel.TabIndex = 1;
            this.PhoneNoLabel.Text = "Podaj numer telefonu:";
            // 
            // FifthViewLabel
            // 
            this.FifthViewLabel.AutoSize = true;
            this.FifthViewLabel.Location = new System.Drawing.Point(29, 24);
            this.FifthViewLabel.Name = "FifthViewLabel";
            this.FifthViewLabel.Size = new System.Drawing.Size(71, 13);
            this.FifthViewLabel.TabIndex = 5;
            this.FifthViewLabel.Text = "Krok czwarty:";
            // 
            // EndViewPanel
            // 
            this.EndViewPanel.Controls.Add(this.EndViewHomeNoLabel);
            this.EndViewPanel.Controls.Add(this.EndViewZipCodeLabel);
            this.EndViewPanel.Controls.Add(this.EndViewStreetLabel);
            this.EndViewPanel.Controls.Add(this.EndViewSurnameLabel);
            this.EndViewPanel.Controls.Add(this.EndViewCityLabel);
            this.EndViewPanel.Controls.Add(this.EndViewPhoneNoLabel);
            this.EndViewPanel.Controls.Add(this.EndNameLabel);
            this.EndViewPanel.Controls.Add(this.EndViewLabel);
            this.EndViewPanel.Location = new System.Drawing.Point(5, 4);
            this.EndViewPanel.Name = "EndViewPanel";
            this.EndViewPanel.Size = new System.Drawing.Size(443, 293);
            this.EndViewPanel.TabIndex = 1;
            this.EndViewPanel.Visible = false;
            // 
            // EndViewHomeNoLabel
            // 
            this.EndViewHomeNoLabel.AutoSize = true;
            this.EndViewHomeNoLabel.Location = new System.Drawing.Point(135, 181);
            this.EndViewHomeNoLabel.Name = "EndViewHomeNoLabel";
            this.EndViewHomeNoLabel.Size = new System.Drawing.Size(49, 13);
            this.EndViewHomeNoLabel.TabIndex = 8;
            this.EndViewHomeNoLabel.Text = "HomeNo";
            // 
            // EndViewZipCodeLabel
            // 
            this.EndViewZipCodeLabel.AutoSize = true;
            this.EndViewZipCodeLabel.Location = new System.Drawing.Point(135, 155);
            this.EndViewZipCodeLabel.Name = "EndViewZipCodeLabel";
            this.EndViewZipCodeLabel.Size = new System.Drawing.Size(47, 13);
            this.EndViewZipCodeLabel.TabIndex = 7;
            this.EndViewZipCodeLabel.Text = "ZipCode";
            // 
            // EndViewStreetLabel
            // 
            this.EndViewStreetLabel.AutoSize = true;
            this.EndViewStreetLabel.Location = new System.Drawing.Point(43, 181);
            this.EndViewStreetLabel.Name = "EndViewStreetLabel";
            this.EndViewStreetLabel.Size = new System.Drawing.Size(35, 13);
            this.EndViewStreetLabel.TabIndex = 6;
            this.EndViewStreetLabel.Text = "Street";
            // 
            // EndViewSurnameLabel
            // 
            this.EndViewSurnameLabel.AutoSize = true;
            this.EndViewSurnameLabel.Location = new System.Drawing.Point(126, 73);
            this.EndViewSurnameLabel.Name = "EndViewSurnameLabel";
            this.EndViewSurnameLabel.Size = new System.Drawing.Size(49, 13);
            this.EndViewSurnameLabel.TabIndex = 5;
            this.EndViewSurnameLabel.Text = "Surname";
            // 
            // EndViewCityLabel
            // 
            this.EndViewCityLabel.AutoSize = true;
            this.EndViewCityLabel.Location = new System.Drawing.Point(43, 155);
            this.EndViewCityLabel.Name = "EndViewCityLabel";
            this.EndViewCityLabel.Size = new System.Drawing.Size(24, 13);
            this.EndViewCityLabel.TabIndex = 4;
            this.EndViewCityLabel.Text = "City";
            // 
            // EndViewPhoneNoLabel
            // 
            this.EndViewPhoneNoLabel.AutoSize = true;
            this.EndViewPhoneNoLabel.Location = new System.Drawing.Point(43, 123);
            this.EndViewPhoneNoLabel.Name = "EndViewPhoneNoLabel";
            this.EndViewPhoneNoLabel.Size = new System.Drawing.Size(52, 13);
            this.EndViewPhoneNoLabel.TabIndex = 3;
            this.EndViewPhoneNoLabel.Text = "PhoneNo";
            // 
            // EndNameLabel
            // 
            this.EndNameLabel.AutoSize = true;
            this.EndNameLabel.Location = new System.Drawing.Point(43, 73);
            this.EndNameLabel.Name = "EndNameLabel";
            this.EndNameLabel.Size = new System.Drawing.Size(35, 13);
            this.EndNameLabel.TabIndex = 1;
            this.EndNameLabel.Text = "Name";
            // 
            // EndViewLabel
            // 
            this.EndViewLabel.AutoSize = true;
            this.EndViewLabel.Location = new System.Drawing.Point(43, 29);
            this.EndViewLabel.Name = "EndViewLabel";
            this.EndViewLabel.Size = new System.Drawing.Size(40, 13);
            this.EndViewLabel.TabIndex = 0;
            this.EndViewLabel.Text = "Koniec";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 391);
            this.Controls.Add(this.ContainerPanel);
            this.Controls.Add(this.ButtonPanel);
            this.Name = "Form1";
            this.Text = "PGS Rekrutacja";
            this.ButtonPanel.ResumeLayout(false);
            this.FirstViewPanel.ResumeLayout(false);
            this.FirstViewPanel.PerformLayout();
            this.ThirdViewPanel.ResumeLayout(false);
            this.ThirdViewPanel.PerformLayout();
            this.SecondViewPanel.ResumeLayout(false);
            this.SecondViewPanel.PerformLayout();
            this.ContainerPanel.ResumeLayout(false);
            this.FourthViewPanel.ResumeLayout(false);
            this.FourthViewPanel.PerformLayout();
            this.FifthViewPanel.ResumeLayout(false);
            this.FifthViewPanel.PerformLayout();
            this.EndViewPanel.ResumeLayout(false);
            this.EndViewPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ButtonPanel;
        private System.Windows.Forms.Button BackButton;
        private System.Windows.Forms.Button NextButton;
        private System.Windows.Forms.Panel FirstViewPanel;
        private System.Windows.Forms.Panel SecondViewPanel;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label NameLable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label SecondViewLabel;
    
      
        public System.Windows.Forms.Panel ContainerPanel;
      
        private System.Windows.Forms.Panel FourthViewPanel;
        private System.Windows.Forms.Label ZipCodeLabel;
        private System.Windows.Forms.Label CityLabel;
        private System.Windows.Forms.Label StreetLabel;
        private System.Windows.Forms.Label AddressLabel;
        private System.Windows.Forms.Label FourthViewLabel;
        private System.Windows.Forms.TextBox ZipCodeTextBox;
        private System.Windows.Forms.TextBox CityTextBox;
        private System.Windows.Forms.TextBox StreetTextBox;
        private System.Windows.Forms.TextBox HomeNoTextBox;
        private System.Windows.Forms.Label HomeNoLabel;
        private System.Windows.Forms.Panel FifthViewPanel;
        private System.Windows.Forms.TextBox PhoneNoTextBox;
        private System.Windows.Forms.Label PhoneNoLabel;
        private System.Windows.Forms.Label FifthViewLabel;
        private System.Windows.Forms.Panel ThirdViewPanel;
        private System.Windows.Forms.Label ThirdViewLabel;
        private System.Windows.Forms.TextBox SurnameTextBox;
        private System.Windows.Forms.Label SurnameLabel;
        private System.Windows.Forms.Panel EndViewPanel;
        private System.Windows.Forms.Label EndViewHomeNoLabel;
        private System.Windows.Forms.Label EndViewZipCodeLabel;
        private System.Windows.Forms.Label EndViewStreetLabel;
        private System.Windows.Forms.Label EndViewSurnameLabel;
        private System.Windows.Forms.Label EndViewCityLabel;
        private System.Windows.Forms.Label EndViewPhoneNoLabel;
        private System.Windows.Forms.Label EndNameLabel;
        private System.Windows.Forms.Label EndViewLabel;
    }
}

