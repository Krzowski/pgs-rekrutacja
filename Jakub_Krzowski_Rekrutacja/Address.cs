﻿using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Jakub_Krzowski_Rekrutacja
{
    public class Address
    {
        public string City { get; set; } 
        public string ZipCode { get; set; } 
        public string HomeNumber { get; set; } 
        public string Street { get; set; } 

        public Address(string city,string Zip,string Home,string street)
        {
            City = city;
            ZipCode = Zip;
            HomeNumber = Home;
            Street = street;
        }

        public bool Validate()
        {
            return ValidateCity() && ValidateStreet() && ValidateHomeNo() && ValidateZipCode();
        }

        private bool ValidateZipCode()
        {
            Match match = Regex.Match(ZipCode, @"(?i)^[0-9-]+$");
            if (!match.Success)
            {
                MessageBox.Show("Błąd w kodzie pocztowym");
                return false;
            }
            return true;
        }

        private bool ValidateHomeNo()
        {
            Match match = Regex.Match(HomeNumber, @"(?i)^[a-z0-9/]+$");
            if (!match.Success)
            {
                MessageBox.Show("Błąd w numerze domu");
                return false;
            }

            return true;
        }

        private bool  ValidateStreet()
        {
            Match match = Regex.Match(Street, @"(?i)^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+$");
            if (!match.Success)
            {
                MessageBox.Show("Bład w nazwie ulicy");
                return false;
            }
            return true;
        }

        private bool ValidateCity()
        {
            Match match = Regex.Match(City, @"(?i)^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+$");
            if (!match.Success)
            {
                MessageBox.Show("Bład w nazwie miasta");
                return false;
            }
            return true;
        }

    }
}